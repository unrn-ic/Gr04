import time


def menu():
    while True:
        try:
            opcion = int(
                input(
                    "Ingrese la opción que desee: \n1. Calculadora Clásica \n2. Calculadora de Fracciones \n3. Calculadora de Conversiones \n4. Salir (Off) \n"
                )
            )
            match opcion:
                case 1:
                    calcu_clasica()
                case 2:
                    calcu_fraccion()
                case 3:
                    calcu_conversora()
                case 4:
                    exit()
                case _:
                    print("Seleccione una opcion válida")
                    continue

        except ValueError:
            print("Ingrese una opcion de la lista: ")
            continue
        break


def funcion_bin(base10):
    num_decimal = base10
    num_binario = ""

    while num_decimal > 0:
        if num_decimal % 2 == 0:
            num_binario = "0" + num_binario
        else:
            num_binario = "1" + num_binario

        num_decimal = int(num_decimal / 2)

    print("{:<5} | {:<5}".format("Base10", "Base2"))
    print("{:<5}  | {:<5}".format(base10, num_binario))

    time.sleep(2)
    print("Volviendo al menú")
    time.sleep(2)
    menu()


def funcion_octal(base10):
    num_decimal = base10
    num_octal = ""
    resto = 0

    while num_decimal > 0:
        resto = num_decimal % 8
        num_decimal = int((num_decimal - resto) / 8)
        num_octal = str(resto) + num_octal

    print("{:<5} | {:<5}".format("Base10", "Base8"))
    print("{:<5}  | {:<5}".format(base10, num_octal))

    time.sleep(2)
    print("Volviendo al menú")
    time.sleep(2)
    menu()


def funcion_hexadecimal(base10):
    resto_letras = ""
    numero_decimal = base10
    num_hexa = ""
    resto = 0

    while numero_decimal > 0:
        resto = numero_decimal % 16
        numero_decimal = int((numero_decimal - resto) / 16)
        match resto:
            case 10:
                resto_letras = "A"
                num_hexa = resto_letras + num_hexa

            case 11:
                resto_letras = "B"
                num_hexa = resto_letras + num_hexa

            case 12:
                resto_letras = "C"
                num_hexa = resto_letras + num_hexa

            case 13:
                resto_letras = "E"
                num_hexa = resto_letras + num_hexa

            case 14:
                resto_letras = "D"
                num_hexa = resto_letras + num_hexa

            case 15:
                resto_letras = "F"
                num_hexa = resto_letras + num_hexa

            case _:
                num_hexa = str(resto) + num_hexa

    print("{:<5} | {:<5}".format("Base10", "Base16"))
    print("{:<5}  | {:<5}".format(base10, num_hexa))

    time.sleep(2)
    print("Volviendo al menú")
    time.sleep(2)
    menu()


def suma_R():
    while True:
        try:
            resultado = " "
            num = float(input("Ingrese un numero Real: "))
            suma = num

        except ValueError:
            print("Ingrese un input válido")
            continue

        break

    while resultado != "=":
        try:
            num = float(input("Ingrese otro numero Real: "))
            suma = suma + num
            resultado = input("Si desea ver el resultado presione =: ")

        except ValueError:
            print("Ingrese un input válido")
            continue

    print("El resultado de la suma es: ", suma)

    time.sleep(2)
    print("Volviendo al menú")
    time.sleep(2)
    menu()


def resta_R():
    while True:
        try:
            resultado = " "
            num = float(input("Ingrese un numero Real: "))
            resta = num

        except ValueError:
            print("Ingrese un input válido")
            continue

        break

    while resultado != "=":
        try:
            num = float(input("Ingrese un numero Real: "))
            resta = resta - num
            resultado = input("Si desea ver el resultado presione =: ")

        except ValueError:
            print("Ingrese un input válido")
            continue

    print("El resultado de la resta es: ", resta)

    time.sleep(2)
    print("Volviendo al menú")
    time.sleep(2)
    menu()


def mult_R():
    while True:
        try:
            multiplicacion = float(input("Ingrese un numero Real: "))
            resultado = " "

        except ValueError:
            print("Ingrese un input válido")
            continue

        break

    while resultado != "=":
        try:
            num = float(input("Ingrese otro numero Real: "))
            multiplicacion = num * multiplicacion
            resultado = input("Si desea ver el resultado presione =: ")

        except ValueError:
            print("Ingrese un input válido")
            continue

    print("El resultado es: ", multiplicacion)

    time.sleep(2)
    print("Volviendo al menú")
    time.sleep(2)
    menu()


def div_R():
    while True:
        try:
            resultado = " "
            num = float(input("Ingrese un numero Real: "))
            division = num

        except ValueError:
            print("Ingrese un input válido")
            continue

        break

    while resultado != "=":
        try:
            num = float(input("Ingrese otro numero: "))

            if num == 0:
                raise Exception

            else:
                division = division / num

            resultado = input("Si desea ver el resultado presione = : ")

        except ValueError:
            print("Ingrese un input correcto")
            continue

        except Exception:
            print("No se puede divir un número por 0, vuelva a ingresar un número Real")
            continue

    print("El resultado de la division es: ", division)

    time.sleep(2)
    print("Volviendo al menú")
    time.sleep(2)
    menu()


def suma_fraccion():
    while True:
        try:
            resultado = " "
            num = int(input("ingrese el numerador de la fraccion: "))
            den = int(input("ingrese el denominador de la fraccion: "))
            if den == 0:
                raise Exception

        except ValueError:
            print("Ingrese un número")
            continue

        except Exception:
            print("El denominador no puede ser 0. Vuelva a ingresar las fracciones")
            continue

        break

    while resultado != "=":
        try:
            numerador = int(input("ingrese el numerador de la fraccion: "))
            denominador = int(input("ingrese el denominador de la fraccion: "))
            if denominador == 0:
                raise Exception

            num = num * denominador + numerador * den
            den = den * denominador

            resultado = input(str("Ingrese = si quiere ver el resultado,sino seguir"))

        except ValueError:
            print("Ingrese un número")
            continue

        except Exception:
            print("El denominador no puede ser 0. Vuelva a ingresar las fracciones")
            continue

    print(f"El resultado es {num} / {den} ")

    time.sleep(2)
    print("Volviendo al menú")
    time.sleep(2)
    menu()


def resta_fraccion():
    while True:
        try:
            resultado = " "
            num = int(input("ingrese el numerador de la fraccion: "))
            den = int(input("ingrese el denominador de la fraccion: "))
            if den == 0:
                raise Exception

        except ValueError:
            print("Ingrese un número")
            continue

        except Exception:
            print("El denominador no puede ser 0. Vuelva a ingresar las fracciones")
            continue

        break

    while resultado != "=":
        try:
            numerador = int(input("ingrese el numerador de la fraccion: "))
            denominador = int(input("ingrese el denominador de la fraccion: "))
            if denominador == 0:
                raise Exception

            num = num * denominador - numerador * den
            den = den * denominador

            resultado = input(str("ingrese = si quiere ver el resultado,sino seguir"))

        except ValueError:
            print("Ingrese un número")
            continue

        except Exception:
            print("El denominador no puede ser 0. Vuelva a ingresar las fracciones")
            continue

    print(f"el resultado es {num} / {den} ")

    time.sleep(2)
    print("Volviendo al menú")
    time.sleep(2)
    menu()


def mult_fraccion():
    while True:
        try:
            resultado = " "
            num = int(input("Ingrese el numerador de la fraccion: "))
            den = int(input("Ingrese el denominador de la fraccion: "))

            if den == 0:
                raise Exception

        except ValueError:
            print("Ingrese un número")
            continue

        except Exception:
            print("El denominador no puede ser 0. Vuelva a ingresar las fracciones")
            continue

        break

    while resultado != "=":
        try:
            numerador = int(input("Ingrese el numerador de la fraccion: "))
            denominador = int(input("Ingrese el denominador de la fraccion: "))

            if denominador == 0:
                raise Exception

            num = num * numerador
            den = den * denominador

            resultado = input(str("Ingrese = si quiere ver el resultado,sino seguir: "))

        except ValueError:
            print("Ingrese un número")
            continue

        except Exception:
            print("El denominador no puede ser 0. Vuelva a ingresar las fracciones")
            continue

    print(f"el resultado es {num} / {den} ")

    time.sleep(2)
    print("Volviendo al menú")
    time.sleep(2)
    menu()


def div_fraccion():
    while True:
        try:
            resultado = " "
            num = int(input("Ingrese el numerador de la fraccion: "))
            den = int(input("Ingrese el denominador de la fraccion: "))

            if den == 0:
                raise Exception

        except ValueError:
            print("Ingrese un número")
            continue

        except Exception:
            print("El denominador no puede ser 0. Vuelva a ingresar las fracciones")
            continue

        break

    while resultado != "=":
        try:
            numerador = int(input("Ingrese el numerador de la fraccion: "))
            denominador = int(input("Ingrese el denominador de la fraccion: "))

            if denominador == 0:
                raise Exception

            num = num * denominador
            den = den * numerador

            resultado = input(str("Ingrese = si quiere ver el resultado,sino seguir: "))

        except ValueError:
            print("Ingrese un número")
            continue

        except Exception:
            print("El denominador no puede ser 0. Vuelva a ingresar las fracciones")
            continue

    print(f"El resultado es {num} / {den}")

    time.sleep(2)
    print("Volviendo al menú")
    time.sleep(2)
    menu()


def calcu_clasica():
    while True:
        try:
            pasaje = int(
                input(
                    "Ingrese la opcion que desee: (1) suma / (2) resta / (3) multiplicacion / (4) division \n"
                )
            )
            match pasaje:
                case 1:
                    suma_R()
                case 2:
                    resta_R()
                case 3:
                    mult_R()
                case 4:
                    div_R()
                case _:
                    print("Ingrese una opcion correcta porfavor.\n")
                    continue

        except ValueError:
            print("Seleccione una opcion correcta")
            continue

        break


def calcu_fraccion():
    while True:
        try:
            pasaje = int(
                input(
                    "Ingrese la opcion que desee: (1) suma / (2) resta / (3) multiplicacion / (4) division \n"
                )
            )
            match pasaje:
                case 1:
                    suma_fraccion()
                case 2:
                    resta_fraccion()
                case 3:
                    mult_fraccion()
                case 4:
                    div_fraccion()
                case _:
                    print("Ingrese una opcion correcta porfavor.\n")
                    continue

        except ValueError:
            print("Seleccione una opcion correcta")
            continue

        break


def calcu_conversora():
    while True:
        try:
            base10 = int(input("Ingrese un numero entero positivo: \n"))
            if base10 < 0:
                raise Exception

        except ValueError:
            print("Ingrese un numero entero positivo")
            continue

        except Exception:
            print("El número tiene que ser positivo")
            continue

        break

    while True:
        try:
            pasaje = int(
                input(
                    "Ingrese la opcion que desee: (1) binario / (2) octal / (3) hexadecimal\n"
                )
            )
            match pasaje:
                case 1:
                    funcion_bin(base10)
                case 2:
                    funcion_octal(base10)
                case 3:
                    funcion_hexadecimal(base10)
                case _:
                    print("Error, elija una opcion correcta.")
                    continue

        except ValueError:
            print("Seleccione una opcion correcta")
            continue

        break


comienzo = "Off"
comienzo = input("Ingrese On para comenzar: ")

if comienzo.lower() == "on":
    menu()
